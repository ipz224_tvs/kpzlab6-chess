﻿namespace ChessBoard
{
    public interface IChessObserver
    {
        void Update();
    }
}
