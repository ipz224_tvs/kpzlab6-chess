﻿using System;
using System.Windows.Forms;

namespace Chess
{
    public partial class Mods : Form
    {
        public Mods()
        {
            InitializeComponent();
        }

        private void buttonStandart_Click(object sender, EventArgs e)
        {
            Game game= new Game();
            this.Close();
            game.Show();
        }

        private void buttonRapid_Click(object sender, EventArgs e)
        {
            Standart standart = new Standart();
            this.Close();
            standart.Show();
        }

        private void buttonEandE_Click(object sender, EventArgs e)
        {
            EandE eande = new EandE();
            this.Close();
            eande.Show();
        }
    }
}
