﻿namespace Chess
{
    partial class Mods
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelMods = new System.Windows.Forms.Label();
            this.buttonRapid = new System.Windows.Forms.Button();
            this.buttonStandart = new System.Windows.Forms.Button();
            this.buttonEandE = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // labelMods
            // 
            this.labelMods.AutoSize = true;
            this.labelMods.BackColor = System.Drawing.Color.Transparent;
            this.labelMods.Font = new System.Drawing.Font("Consolas", 22.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelMods.ForeColor = System.Drawing.Color.SkyBlue;
            this.labelMods.Location = new System.Drawing.Point(235, 208);
            this.labelMods.Name = "labelMods";
            this.labelMods.Size = new System.Drawing.Size(339, 43);
            this.labelMods.TabIndex = 0;
            this.labelMods.Text = "Вибір режиму гри";
            // 
            // buttonRapid
            // 
            this.buttonRapid.BackgroundImage = global::Chess.Properties.Resources.butmod;
            this.buttonRapid.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.buttonRapid.Font = new System.Drawing.Font("Consolas", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonRapid.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.buttonRapid.Location = new System.Drawing.Point(643, 185);
            this.buttonRapid.Name = "buttonRapid";
            this.buttonRapid.Size = new System.Drawing.Size(331, 66);
            this.buttonRapid.TabIndex = 1;
            this.buttonRapid.Text = "Рапід (5 хв)";
            this.buttonRapid.UseVisualStyleBackColor = true;
            this.buttonRapid.Click += new System.EventHandler(this.buttonRapid_Click);
            // 
            // buttonStandart
            // 
            this.buttonStandart.BackgroundImage = global::Chess.Properties.Resources.butmod;
            this.buttonStandart.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.buttonStandart.Font = new System.Drawing.Font("Consolas", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonStandart.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.buttonStandart.Location = new System.Drawing.Point(643, 49);
            this.buttonStandart.Name = "buttonStandart";
            this.buttonStandart.Size = new System.Drawing.Size(331, 66);
            this.buttonStandart.TabIndex = 2;
            this.buttonStandart.Text = "Стандартний (без часу)";
            this.buttonStandart.UseVisualStyleBackColor = true;
            this.buttonStandart.Click += new System.EventHandler(this.buttonStandart_Click);
            // 
            // buttonEandE
            // 
            this.buttonEandE.BackgroundImage = global::Chess.Properties.Resources.butmod2;
            this.buttonEandE.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.buttonEandE.Font = new System.Drawing.Font("Consolas", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonEandE.ForeColor = System.Drawing.SystemColors.InactiveBorder;
            this.buttonEandE.Location = new System.Drawing.Point(207, 443);
            this.buttonEandE.Name = "buttonEandE";
            this.buttonEandE.Size = new System.Drawing.Size(331, 66);
            this.buttonEandE.TabIndex = 3;
            this.buttonEandE.Text = "\"Битва Слонів\"";
            this.buttonEandE.UseVisualStyleBackColor = true;
            this.buttonEandE.Click += new System.EventHandler(this.buttonEandE_Click);
            // 
            // Mods
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::Chess.Properties.Resources.Mods;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1052, 668);
            this.Controls.Add(this.buttonEandE);
            this.Controls.Add(this.buttonStandart);
            this.Controls.Add(this.buttonRapid);
            this.Controls.Add(this.labelMods);
            this.Name = "Mods";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Mods";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelMods;
        private System.Windows.Forms.Button buttonRapid;
        private System.Windows.Forms.Button buttonStandart;
        private System.Windows.Forms.Button buttonEandE;
    }
}