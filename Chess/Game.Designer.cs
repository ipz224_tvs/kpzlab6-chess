﻿namespace Chess
{
    partial class Game
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Game));
            this.flowLayoutPanelWhite = new System.Windows.Forms.FlowLayoutPanel();
            this.label1 = new System.Windows.Forms.Label();
            this.flowLayoutPanelBlack = new System.Windows.Forms.FlowLayoutPanel();
            this.label2 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label_8 = new System.Windows.Forms.Label();
            this.label_7 = new System.Windows.Forms.Label();
            this.label_6 = new System.Windows.Forms.Label();
            this.label_5 = new System.Windows.Forms.Label();
            this.label_4 = new System.Windows.Forms.Label();
            this.label_3 = new System.Windows.Forms.Label();
            this.label_2 = new System.Windows.Forms.Label();
            this.label_1 = new System.Windows.Forms.Label();
            this.label_a = new System.Windows.Forms.Label();
            this.label_b = new System.Windows.Forms.Label();
            this.label_c = new System.Windows.Forms.Label();
            this.label_d = new System.Windows.Forms.Label();
            this.label_e = new System.Windows.Forms.Label();
            this.label_f = new System.Windows.Forms.Label();
            this.label_g = new System.Windows.Forms.Label();
            this.label_h = new System.Windows.Forms.Label();
            this.buttonPass = new System.Windows.Forms.Button();
            this.labelMove = new System.Windows.Forms.Label();
            this.imageListWhite = new System.Windows.Forms.ImageList(this.components);
            this.imageListBlack = new System.Windows.Forms.ImageList(this.components);
            this.button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // flowLayoutPanelWhite
            // 
            this.flowLayoutPanelWhite.Location = new System.Drawing.Point(759, 79);
            this.flowLayoutPanelWhite.Margin = new System.Windows.Forms.Padding(4);
            this.flowLayoutPanelWhite.Name = "flowLayoutPanelWhite";
            this.flowLayoutPanelWhite.Size = new System.Drawing.Size(267, 265);
            this.flowLayoutPanelWhite.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(756, 58);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(121, 18);
            this.label1.TabIndex = 2;
            this.label1.Text = "Здобуто білих";
            this.label1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // flowLayoutPanelBlack
            // 
            this.flowLayoutPanelBlack.Location = new System.Drawing.Point(759, 400);
            this.flowLayoutPanelBlack.Margin = new System.Windows.Forms.Padding(4);
            this.flowLayoutPanelBlack.Name = "flowLayoutPanelBlack";
            this.flowLayoutPanelBlack.Size = new System.Drawing.Size(267, 265);
            this.flowLayoutPanelBlack.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(756, 379);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(134, 18);
            this.label2.TabIndex = 3;
            this.label2.Text = "Здобуто чорних";
            this.label2.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Location = new System.Drawing.Point(25, 60);
            this.panel1.Margin = new System.Windows.Forms.Padding(0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(707, 632);
            this.panel1.TabIndex = 3;
            // 
            // label_8
            // 
            this.label_8.Location = new System.Drawing.Point(8, 60);
            this.label_8.Margin = new System.Windows.Forms.Padding(0);
            this.label_8.Name = "label_8";
            this.label_8.Size = new System.Drawing.Size(17, 76);
            this.label_8.TabIndex = 4;
            this.label_8.Text = "8";
            this.label_8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label_7
            // 
            this.label_7.Location = new System.Drawing.Point(8, 150);
            this.label_7.Margin = new System.Windows.Forms.Padding(0);
            this.label_7.Name = "label_7";
            this.label_7.Size = new System.Drawing.Size(17, 76);
            this.label_7.TabIndex = 5;
            this.label_7.Text = "7";
            this.label_7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label_6
            // 
            this.label_6.Location = new System.Drawing.Point(8, 226);
            this.label_6.Margin = new System.Windows.Forms.Padding(0);
            this.label_6.Name = "label_6";
            this.label_6.Size = new System.Drawing.Size(17, 76);
            this.label_6.TabIndex = 5;
            this.label_6.Text = "6";
            this.label_6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label_5
            // 
            this.label_5.Location = new System.Drawing.Point(8, 302);
            this.label_5.Margin = new System.Windows.Forms.Padding(0);
            this.label_5.Name = "label_5";
            this.label_5.Size = new System.Drawing.Size(17, 76);
            this.label_5.TabIndex = 5;
            this.label_5.Text = "5";
            this.label_5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label_4
            // 
            this.label_4.Location = new System.Drawing.Point(8, 387);
            this.label_4.Margin = new System.Windows.Forms.Padding(0);
            this.label_4.Name = "label_4";
            this.label_4.Size = new System.Drawing.Size(17, 76);
            this.label_4.TabIndex = 5;
            this.label_4.Text = "4";
            this.label_4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label_3
            // 
            this.label_3.Location = new System.Drawing.Point(8, 476);
            this.label_3.Margin = new System.Windows.Forms.Padding(0);
            this.label_3.Name = "label_3";
            this.label_3.Size = new System.Drawing.Size(17, 76);
            this.label_3.TabIndex = 5;
            this.label_3.Text = "3";
            this.label_3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label_2
            // 
            this.label_2.Location = new System.Drawing.Point(8, 552);
            this.label_2.Margin = new System.Windows.Forms.Padding(0);
            this.label_2.Name = "label_2";
            this.label_2.Size = new System.Drawing.Size(17, 76);
            this.label_2.TabIndex = 5;
            this.label_2.Text = "2";
            this.label_2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label_1
            // 
            this.label_1.Location = new System.Drawing.Point(8, 628);
            this.label_1.Margin = new System.Windows.Forms.Padding(0);
            this.label_1.Name = "label_1";
            this.label_1.Size = new System.Drawing.Size(17, 76);
            this.label_1.TabIndex = 6;
            this.label_1.Text = "1";
            this.label_1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label_a
            // 
            this.label_a.Location = new System.Drawing.Point(22, 724);
            this.label_a.Margin = new System.Windows.Forms.Padding(0);
            this.label_a.Name = "label_a";
            this.label_a.Size = new System.Drawing.Size(83, 16);
            this.label_a.TabIndex = 4;
            this.label_a.Text = "a";
            this.label_a.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label_b
            // 
            this.label_b.Location = new System.Drawing.Point(114, 724);
            this.label_b.Margin = new System.Windows.Forms.Padding(0);
            this.label_b.Name = "label_b";
            this.label_b.Size = new System.Drawing.Size(83, 16);
            this.label_b.TabIndex = 4;
            this.label_b.Text = "b";
            this.label_b.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label_c
            // 
            this.label_c.Location = new System.Drawing.Point(208, 724);
            this.label_c.Margin = new System.Windows.Forms.Padding(0);
            this.label_c.Name = "label_c";
            this.label_c.Size = new System.Drawing.Size(83, 16);
            this.label_c.TabIndex = 4;
            this.label_c.Text = "c";
            this.label_c.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label_d
            // 
            this.label_d.Location = new System.Drawing.Point(291, 724);
            this.label_d.Margin = new System.Windows.Forms.Padding(0);
            this.label_d.Name = "label_d";
            this.label_d.Size = new System.Drawing.Size(83, 16);
            this.label_d.TabIndex = 4;
            this.label_d.Text = "d";
            this.label_d.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label_e
            // 
            this.label_e.Location = new System.Drawing.Point(388, 724);
            this.label_e.Margin = new System.Windows.Forms.Padding(0);
            this.label_e.Name = "label_e";
            this.label_e.Size = new System.Drawing.Size(83, 16);
            this.label_e.TabIndex = 4;
            this.label_e.Text = "e";
            this.label_e.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label_f
            // 
            this.label_f.Location = new System.Drawing.Point(471, 724);
            this.label_f.Margin = new System.Windows.Forms.Padding(0);
            this.label_f.Name = "label_f";
            this.label_f.Size = new System.Drawing.Size(83, 16);
            this.label_f.TabIndex = 4;
            this.label_f.Text = "f";
            this.label_f.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label_g
            // 
            this.label_g.Location = new System.Drawing.Point(554, 724);
            this.label_g.Margin = new System.Windows.Forms.Padding(0);
            this.label_g.Name = "label_g";
            this.label_g.Size = new System.Drawing.Size(83, 16);
            this.label_g.TabIndex = 4;
            this.label_g.Text = "g";
            this.label_g.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label_h
            // 
            this.label_h.Location = new System.Drawing.Point(649, 724);
            this.label_h.Margin = new System.Windows.Forms.Padding(0);
            this.label_h.Name = "label_h";
            this.label_h.Size = new System.Drawing.Size(83, 16);
            this.label_h.TabIndex = 4;
            this.label_h.Text = "h";
            this.label_h.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // buttonPass
            // 
            this.buttonPass.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonPass.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonPass.Location = new System.Drawing.Point(446, 12);
            this.buttonPass.Name = "buttonPass";
            this.buttonPass.Size = new System.Drawing.Size(123, 30);
            this.buttonPass.TabIndex = 7;
            this.buttonPass.Text = "Передати хід";
            this.buttonPass.UseVisualStyleBackColor = false;
            this.buttonPass.Click += new System.EventHandler(this.buttonPass_Click);
            // 
            // labelMove
            // 
            this.labelMove.AutoSize = true;
            this.labelMove.Location = new System.Drawing.Point(637, 20);
            this.labelMove.Name = "labelMove";
            this.labelMove.Size = new System.Drawing.Size(0, 16);
            this.labelMove.TabIndex = 8;
            // 
            // imageListWhite
            // 
            this.imageListWhite.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageListWhite.ImageStream")));
            this.imageListWhite.TransparentColor = System.Drawing.Color.Transparent;
            this.imageListWhite.Images.SetKeyName(0, "WhitePawn.png");
            this.imageListWhite.Images.SetKeyName(1, "WhiteRook.png");
            this.imageListWhite.Images.SetKeyName(2, "WhiteHorse.png");
            this.imageListWhite.Images.SetKeyName(3, "WhiteBishop.png");
            this.imageListWhite.Images.SetKeyName(4, "WhiteQueen.png");
            this.imageListWhite.Images.SetKeyName(5, "WhiteKing.png");
            // 
            // imageListBlack
            // 
            this.imageListBlack.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageListBlack.ImageStream")));
            this.imageListBlack.TransparentColor = System.Drawing.Color.Transparent;
            this.imageListBlack.Images.SetKeyName(0, "BlackPawn.png");
            this.imageListBlack.Images.SetKeyName(1, "BlackRook.png");
            this.imageListBlack.Images.SetKeyName(2, "BlackHorse.png");
            this.imageListBlack.Images.SetKeyName(3, "BlackBishop.png");
            this.imageListBlack.Images.SetKeyName(4, "BlackQueen.png");
            this.imageListBlack.Images.SetKeyName(5, "BlackKing.png");
            // 
            // button1
            // 
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Location = new System.Drawing.Point(931, 12);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(134, 29);
            this.button1.TabIndex = 9;
            this.button1.Text = "В головне меню";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // Game
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1079, 746);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.labelMove);
            this.Controls.Add(this.buttonPass);
            this.Controls.Add(this.label_h);
            this.Controls.Add(this.label_g);
            this.Controls.Add(this.label_f);
            this.Controls.Add(this.label_e);
            this.Controls.Add(this.label_d);
            this.Controls.Add(this.label_c);
            this.Controls.Add(this.label_b);
            this.Controls.Add(this.label_a);
            this.Controls.Add(this.label_1);
            this.Controls.Add(this.label_2);
            this.Controls.Add(this.label_3);
            this.Controls.Add(this.label_4);
            this.Controls.Add(this.label_5);
            this.Controls.Add(this.label_6);
            this.Controls.Add(this.label_7);
            this.Controls.Add(this.label_8);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.flowLayoutPanelBlack);
            this.Controls.Add(this.flowLayoutPanelWhite);
            this.KeyPreview = true;
            this.MaximumSize = new System.Drawing.Size(1097, 793);
            this.MinimumSize = new System.Drawing.Size(1097, 793);
            this.Name = "Game";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Шахмати";
            this.Load += new System.EventHandler(this.Grid_Button_Click);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanelWhite;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanelBlack;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label_8;
        private System.Windows.Forms.Label label_7;
        private System.Windows.Forms.Label label_6;
        private System.Windows.Forms.Label label_5;
        private System.Windows.Forms.Label label_4;
        private System.Windows.Forms.Label label_3;
        private System.Windows.Forms.Label label_2;
        private System.Windows.Forms.Label label_1;
        private System.Windows.Forms.Label label_a;
        private System.Windows.Forms.Label label_b;
        private System.Windows.Forms.Label label_c;
        private System.Windows.Forms.Label label_d;
        private System.Windows.Forms.Label label_e;
        private System.Windows.Forms.Label label_f;
        private System.Windows.Forms.Label label_g;
        private System.Windows.Forms.Label label_h;
        private System.Windows.Forms.Button buttonPass;
        private System.Windows.Forms.Label labelMove;
        private System.Windows.Forms.ImageList imageListWhite;
        private System.Windows.Forms.ImageList imageListBlack;
        private System.Windows.Forms.Button button1;
    }
}

